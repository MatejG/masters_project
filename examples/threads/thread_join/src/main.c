/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <sys/printk.h>

#include <drivers/gpio.h>


void threadA(void *dummy1, void *dummy2, void *dummy3);
void control_thread(void *dummy1, void *dummy2, void *dummy3);


/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   5000
#define CONTROL_TIME_MS 500
#define MY_STACK_SIZE 512
#define PRIO_A 3
#define PRIO_CONTROL 4
#define MY_FLAGS 0

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0	DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN	DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS	DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0	""
#define PIN	0
#define FLAGS	0
#endif


static struct k_thread thread_desc[2];
static k_tid_t threadA_id;
static k_tid_t control_thread_id;

static const struct device *dev;

K_THREAD_STACK_DEFINE(stack_desc, MY_STACK_SIZE);
K_THREAD_STACK_DEFINE(stack_desc2, MY_STACK_SIZE);



void main(void)
{
	int ret;



	dev = device_get_binding(LED0);
	if (dev == NULL) {
		return;
	}

	ret = gpio_pin_configure(dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	if (ret < 0) {
		return;
	}

	threadA_id = k_thread_create(&thread_desc[0], stack_desc, K_THREAD_STACK_SIZEOF(stack_desc), threadA,
	NULL,NULL,NULL, PRIO_A, MY_FLAGS, K_FOREVER);
	control_thread_id = k_thread_create(&thread_desc[1], stack_desc2, K_THREAD_STACK_SIZEOF(stack_desc2), control_thread,
	NULL,NULL,NULL, PRIO_CONTROL, MY_FLAGS, K_FOREVER);
	printk("Bok %s\n", CONFIG_BOARD);


	for (size_t i = 0; i < 2; i++)
	{
		k_thread_start(&thread_desc[i]);
	}
	
	printk("Zapocete dretve\n");

}



void threadA(void *dummy1, void *dummy2, void *dummy3)
{


	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);
	bool flag = true;


	for (size_t i = 0; i < 5; i++)	
	{
		printk("Pozdrav svijete od threadA\n");

		if(flag)
		{
			flag = false;
			k_thread_join(&thread_desc[1], K_FOREVER);
			printk("Vratio se sime\n");

		}
	}
}

void control_thread(void *dummy1, void *dummy2, void *dummy3)
{

	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);

	bool led_is_on = true;

	for (size_t i = 0; i < 10; i++)
	{
		gpio_pin_set(dev, PIN, (int)led_is_on);
		led_is_on = !led_is_on;
		k_msleep(500);
		gpio_pin_set(dev, PIN, (int)led_is_on);
	}
	
	

	printk("Zapocinjem kontrolu al izmjenjeno %d\n", (uint32_t *)(dev -> data));

	

}
