/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <sys/printk.h>

#include <drivers/gpio.h>


void threadA(void *dummy1, void *dummy2, void *dummy3);

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   2000
#define MY_STACK_SIZE 1024
#define MY_PRIO 7

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0	DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN	DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS	DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0	""
#define PIN	0
#define FLAGS	0
#endif


static struct k_thread thread_desc[2];
static k_tid_t threadA_id;
const struct device *dev;

K_THREAD_STACK_DEFINE(stack_desc, MY_STACK_SIZE);


void main(void)
{
	int ret;



	dev = device_get_binding(LED0);
	if (dev == NULL) {
		return;
	}

	ret = gpio_pin_configure(dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	if (ret < 0) {
		return;
	}

	threadA_id = k_thread_create(&thread_desc[0], stack_desc, K_THREAD_STACK_SIZEOF(stack_desc), threadA,
	NULL,NULL,NULL, MY_PRIO, 0, K_NO_WAIT);
	printk("Bok %s\n", CONFIG_BOARD);


	k_thread_start(&thread_desc[0]);
	k_thread_join(&thread_desc[0], K_FOREVER);

	//for(;;);  U main funkciji nebi trebala biti beskonacna petlja ako se koriste dretve, jer se inace ne izvrsavaju
}



void threadA(void *dummy1, void *dummy2, void *dummy3){

	bool led_is_on = true;


	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);

	while (1)
	{
		gpio_pin_set(dev, PIN, (int)led_is_on);
		led_is_on = !led_is_on;
		printk("Pozdrav svijete %s\n", CONFIG_BOARD);

		k_msleep(SLEEP_TIME_MS);
	}
	
	


}
