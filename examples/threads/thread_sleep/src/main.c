/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <sys/printk.h>

#include <drivers/gpio.h>


void control_thread(void *dummy1, void *dummy2, void *dummy3);


/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   2000
#define CONTROL_TIME_MS 500
#define MY_STACK_SIZE 512
#define MY_PRIO 7
#define MY_FLAGS 0

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0	DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN	DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS	DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0	""
#define PIN	0
#define FLAGS	0
#endif


static struct k_thread thread_desc;
static k_tid_t threadA_id;
static k_tid_t control_thread_id;

static const struct device *dev;

K_THREAD_STACK_DEFINE(stack_desc, MY_STACK_SIZE);



void main(void)
{
	int ret;



	dev = device_get_binding(LED0);
	if (dev == NULL) {
		return;
	}

	ret = gpio_pin_configure(dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	if (ret < 0) {
		return;
	}

	control_thread_id = k_thread_create(&thread_desc, stack_desc, K_THREAD_STACK_SIZEOF(stack_desc), control_thread,
	NULL,NULL,NULL, 6, MY_FLAGS, K_FOREVER);
	printk("Bok %s\n", CONFIG_BOARD);


	
	k_thread_start(&thread_desc);
	
	printk("Zapocete dretve\n");

}




void control_thread(void *dummy1, void *dummy2, void *dummy3)
{

	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);

	bool led_is_on = true;


	printk("Zapocinjem kontrolu\n");

	while(1)
	{
		gpio_pin_set(dev, PIN, (int)led_is_on);
		led_is_on = !led_is_on;
		k_sleep(K_MSEC(1000));
		printk("Bok\n");

		
	}

}
