/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <sys/printk.h>

#include <drivers/gpio.h>

/************* Private function declarations ***************************/
static void threadA(void *dummy1, void *dummy2, void *dummy3);
static void init_thread(void *dummy1, void *dummy2, void *dummy3);


/************* Private Macros ***************************/

#define PERIODIC_WAIT_TIME_MS (2000u)
#define MY_STACK_SIZE (1024u)
#define MY_PRIO (7u)
#define START_DELAY_MS (1000u)





/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0	DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN	DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS	DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0	""
#define PIN	0
#define FLAGS	0
#endif


//static struct k_thread thread_desc[2];
const struct device *dev;


static K_THREAD_DEFINE(thread_A_id, MY_STACK_SIZE,
                threadA, NULL, NULL, NULL,
                MY_PRIO, 0, START_DELAY_MS);

static K_THREAD_DEFINE(init_thread_id, MY_STACK_SIZE,
				 init_thread, NULL, NULL, NULL, 
				 MY_PRIO, 0, 0);


static void threadA(void *dummy1, void *dummy2, void *dummy3){

	bool led_is_on = true;


	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);

	while (1)
	{
		gpio_pin_set(dev, PIN, (int)led_is_on);
		led_is_on = !led_is_on;
		printk("Pozdrav svijete od threadA\n");

		k_msleep(PERIODIC_WAIT_TIME_MS);
	}
}


static void init_thread(void *dummy1, void *dummy2, void *dummy3)
{
	int ret;

	printk("Inicijalizacijska dretva za razvojnu plocicu: %s\n", CONFIG_BOARD);

	dev = device_get_binding(LED0);
	if (dev == NULL) {
		return;
	}

	ret = gpio_pin_configure(dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	if (ret < 0) {
		return;
	}


}


