/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <sys/printk.h>

#include <drivers/gpio.h>

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS 5000
#define CONTROL_TIME_MS 500
#define MY_STACK_SIZE 512
#define PRIO_A 3
#define PRIO_CONTROL 4
#define MY_FLAGS 0

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0 DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0 ""
#define PIN 0
#define FLAGS 0
#endif

#define LED1_NODE DT_NODELABEL(blue_led_6) //if we are not using aliases, we have to use DT_NODELABEL
#define LED1 DT_GPIO_LABEL(LED1_NODE, gpios)
#define BLUE_PIN DT_GPIO_PIN(LED1_NODE, gpios)
#define BLUE_FLAGS DT_GPIO_FLAGS(LED1_NODE, gpios)


static const struct device *dev;
static const struct device *dev2;
static const struct device *buz;

// get the buzzer node identifier

#define BUZZER_NODE DT_NODELABEL(buzzer)
#define BUZZER_PORT DT_GPIO_LABEL(BUZZER_NODE, gpios)
#define BUZZER_PIN DT_GPIO_PIN(BUZZER_NODE, gpios)


void main(void)
{
	int ret;

	dev = device_get_binding(LED0);
	dev2 = device_get_binding(LED1);
	buz = device_get_binding(BUZZER_PORT);
	

	ret = gpio_pin_configure(dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	ret = gpio_pin_configure(dev2, BLUE_PIN, GPIO_OUTPUT_ACTIVE | BLUE_FLAGS);
	ret = gpio_pin_configure(buz, BUZZER_PIN, GPIO_OUTPUT_INACTIVE);

	if (ret < 0)
	{
		return;
	}

	
#ifndef MUTE
	gpio_pin_set(buz, BUZZER_PIN, true);
	gpio_pin_set(dev,PIN,true);
	gpio_pin_set(dev2,BLUE_PIN,true);

	k_msleep(500);
	gpio_pin_set(buz, BUZZER_PIN, false);
	gpio_pin_set(dev,PIN,false);
	gpio_pin_set(dev2,BLUE_PIN,false);
#endif

	while(1);
}
