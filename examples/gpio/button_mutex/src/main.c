/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <sys/printk.h>

#include <drivers/gpio.h>

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS 5000
#define CONTROL_TIME_MS 500
#define PRIO_A 3
#define PRIO_CONTROL 4

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0 DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0 ""
#define PIN 0
#define FLAGS 0
#endif

#define LED1_NODE DT_NODELABEL(blue_led_6) //if we are not using aliases, we have to use DT_NODELABEL
#define LED1 DT_GPIO_LABEL(LED1_NODE, gpios)
#define BLUE_PIN DT_GPIO_PIN(LED1_NODE, gpios)
#define BLUE_FLAGS DT_GPIO_FLAGS(LED1_NODE, gpios)



// get the buzzer node identifier

#define BUZZER_NODE DT_NODELABEL(buzzer)
#define BUZZER_PORT DT_GPIO_LABEL(BUZZER_NODE, gpios)
#define BUZZER_PIN DT_GPIO_PIN(BUZZER_NODE, gpios)

#define BUTTON_NODE DT_ALIAS(sw0)
#define BUTTON_GPIO_PORT DT_GPIO_LABEL(BUTTON_NODE,gpios)
#define BUTTON_PIN DT_GPIO_PIN(BUTTON_NODE,gpios)




#define STACK_SIZE 256
#define PRIORITY 5

extern void my_entry_point(void *, void *, void *);

K_THREAD_STACK_DEFINE(stack_desc, STACK_SIZE);

K_THREAD_STACK_DEFINE(polling_stack, STACK_SIZE);

struct k_thread thread_desc;
struct k_thread polling_desc;



#define DEKLARACIJA(ime_var) static volatile uint32_t (ime_var)

DEKLARACIJA(speed);
const static uint32_t speed_arr[5] = {2000, 1000, 500, 125, 50};


static const struct device *dev;
static const struct device *dev2;
static const struct device *buz;



void polling_thread(void * __button, void * __pin, void * dummy);
void work_thread(void * dummy1, void * dummy2, void * dummy3);



struct k_mutex mutex;


void main(void)
{
	int ret;



	
	dev = device_get_binding(LED0);
	dev2 = device_get_binding(LED1);
	buz = device_get_binding(BUZZER_PORT);

	

	const struct device *button;
	button = device_get_binding(BUTTON_GPIO_PORT);

	gpio_pin_t button_pin = BUTTON_PIN;

	speed = 1000;
	

	ret = gpio_pin_configure(dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	ret = gpio_pin_configure(dev2, BLUE_PIN, GPIO_OUTPUT_ACTIVE | BLUE_FLAGS);
	ret = gpio_pin_configure(buz, BUZZER_PIN, GPIO_OUTPUT_INACTIVE);
	ret = gpio_pin_configure(button, BUTTON_PIN, GPIO_INPUT);

	if (ret < 0)
	{
		return;
	}

	
#ifndef MUTE
	gpio_pin_set(buz, BUZZER_PIN, true);
	gpio_pin_set(dev,PIN,true);
	gpio_pin_set(dev2,BLUE_PIN,true);

	k_msleep(500);
	gpio_pin_set(buz, BUZZER_PIN, false);
	gpio_pin_set(dev,PIN,false);
	gpio_pin_set(dev2,BLUE_PIN,false);
#endif

	k_mutex_init(&mutex);



	k_tid_t tid_work = k_thread_create(&thread_desc, stack_desc,
                                 K_THREAD_STACK_SIZEOF(stack_desc),
                                 work_thread,
                                 NULL, NULL, NULL,
                                 PRIORITY, 0, K_FOREVER);

	k_tid_t tid_polling = k_thread_create(&polling_desc, polling_stack,
                                 K_THREAD_STACK_SIZEOF(polling_stack),
                                 polling_thread,
                                 (void *)button,(void *) &button_pin , NULL,
                                 PRIORITY, 0, K_FOREVER);	


    ARG_UNUSED(tid_work);
	ARG_UNUSED(tid_polling);					 

							
	k_thread_start(&thread_desc);
	k_thread_start(&polling_desc);

}





void work_thread(void * dummy1, void * dummy2, void * dummy3)
{

	int32_t status = true;

	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);

	uint32_t wait_time;

	while(true)
	{
		k_mutex_lock(&mutex,K_FOREVER);
		wait_time = speed;
		k_mutex_unlock(&mutex);


		k_sleep(K_MSEC(wait_time));
//		gpio_pin_set(buz, BUZZER_PIN, status);
		gpio_pin_set(dev,PIN,status);
		gpio_pin_set(dev2,BLUE_PIN,status);
		status = !status;

	}

}


void polling_thread(void * __button, void * __pin, void * dummy)
{
	uint32_t i = 0;
	struct device *button;
	gpio_pin_t pin = *(gpio_pin_t *) __pin;
	button = (struct device *) __button;

	ARG_UNUSED(dummy);

	while(true)
	{

		if(gpio_pin_get(button,pin) == true)
		{
			k_sleep(K_MSEC(50));
			if(gpio_pin_get(button,pin) == true)
			{
				i = (i+1) % 5;
				k_mutex_lock(&mutex, K_FOREVER);
				speed = speed_arr[i];
				k_mutex_unlock(&mutex);


			}
		}

	}


}
