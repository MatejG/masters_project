#define STACK_SIZE 256
#define PRIORITY 5
#define MSGQ_SIZE 10



typedef enum _event_t{
	BUTTON_PRESS,
	LED_IN_LAST,
	SOMETHING_ELSE
}event_t;









void polling_thread(void *__button, void *__pin, void *dummy);
void work_thread(void *dummy1, void *dummy2, void *dummy3);
void ISR_button_handler(const struct device *port, struct gpio_callback *cb, gpio_port_pins_t pins);
static int configure_button();
void ISR_timer_button_debounce(struct k_timer *dummy);