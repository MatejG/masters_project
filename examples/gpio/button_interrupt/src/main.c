/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <sys/printk.h>
#include <kernel.h>


#include <drivers/gpio.h>
#include "main.h"


/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0 DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0 ""
#define PIN 0
#define FLAGS 0
#endif

#define LED1_NODE DT_NODELABEL(blue_led_6) //if we are not using aliases, we have to use DT_NODELABEL
#define LED1 DT_GPIO_LABEL(LED1_NODE, gpios)
#define BLUE_PIN DT_GPIO_PIN(LED1_NODE, gpios)
#define BLUE_FLAGS DT_GPIO_FLAGS(LED1_NODE, gpios)

// get the buzzer node identifier

#define BUZZER_NODE DT_NODELABEL(buzzer)
#define BUZZER_PORT DT_GPIO_LABEL(BUZZER_NODE, gpios)
#define BUZZER_PIN DT_GPIO_PIN(BUZZER_NODE, gpios)
#define BUTTON_NODE DT_ALIAS(sw0)






K_THREAD_STACK_DEFINE(stack_desc, STACK_SIZE);
K_THREAD_STACK_DEFINE(polling_stack, STACK_SIZE);

struct k_thread thread_desc;
struct k_thread polling_desc;

#define DEKLARACIJA(ime_var) static volatile uint32_t(ime_var)

DEKLARACIJA(speed);
const static uint32_t speed_arr[6] = {2000, 1000, 500, 125, 50,10};

static const struct device *dev;
static const struct device *dev2;
static const struct device *buz;

static const struct gpio_dt_spec red_led_spec = GPIO_DT_SPEC_GET_OR(DT_ALIAS(led2), gpios, {0});
static const struct gpio_dt_spec orange_led_spec = GPIO_DT_SPEC_GET_OR(DT_ALIAS(led1),gpios,{0});

static const struct gpio_dt_spec button_gpio_spec = GPIO_DT_SPEC_GET_OR(BUTTON_NODE, gpios, {0});
static struct gpio_callback button_callback;


struct k_mutex mutex;
static volatile uint32_t debounce_counter;
static volatile uint32_t speed_index;


char __aligned(4) msgq_buffer[MSGQ_SIZE * sizeof(event_t)];

struct k_msgq event_queue;



static int configure_button()
{

	debounce_counter = 0;
	speed_index = 0;

	if ((gpio_pin_configure_dt(&button_gpio_spec, GPIO_INPUT)) != 0)
	{
		gpio_pin_set_dt(&red_led_spec, true);
		return (-2);
	}

	if ((gpio_pin_interrupt_configure_dt(&button_gpio_spec, GPIO_INT_EDGE_RISING)) != 0)
	{
		gpio_pin_set_dt(&red_led_spec, true);
		return (-3);
	}

	//initialize callback structure properly
	gpio_init_callback(&button_callback, ISR_button_handler,
					   BIT(button_gpio_spec.pin));
	gpio_add_callback(button_gpio_spec.port, &button_callback);
	return (0);
}

void main(void)
{
	int ret;

	dev = device_get_binding(LED0);
	dev2 = device_get_binding(LED1);
	buz = device_get_binding(BUZZER_PORT);

	speed = 1000;

	ret = gpio_pin_configure(dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	ret = gpio_pin_configure(dev2, BLUE_PIN, GPIO_OUTPUT_ACTIVE | BLUE_FLAGS);
	ret = gpio_pin_configure(buz, BUZZER_PIN, GPIO_OUTPUT_INACTIVE);

	gpio_pin_configure_dt(&red_led_spec, GPIO_OUTPUT_INACTIVE);
	gpio_pin_configure_dt(&orange_led_spec, GPIO_OUTPUT_INACTIVE);


	if (ret < 0)
	{
		return;
	}
	k_msgq_init(&event_queue, msgq_buffer, sizeof(event_t), MSGQ_SIZE);


	(void)configure_button();

#ifndef MUTE
	gpio_pin_set(buz, BUZZER_PIN, true);
	gpio_pin_set(dev, PIN, true);
	gpio_pin_set(dev2,BLUE_PIN,true);

	k_msleep(500);
    gpio_pin_set(buz, BUZZER_PIN, false);
	gpio_pin_set(dev, PIN, false);
	gpio_pin_set(dev2, BLUE_PIN, false);
#endif

	k_mutex_init(&mutex);

	k_tid_t tid_work = k_thread_create(&thread_desc, stack_desc,
									   K_THREAD_STACK_SIZEOF(stack_desc),
									   work_thread,
									   NULL, NULL, NULL,
									   PRIORITY, 0, K_FOREVER);

	ARG_UNUSED(tid_work);

	k_thread_start(&thread_desc);
}



void work_thread(void *dummy1, void *dummy2, void *dummy3)
{

	int32_t status = true;

	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);

	uint32_t wait_time;
	event_t received_event;

	while (true)
	{
		
		if(k_msgq_get(&event_queue,&received_event,K_NO_WAIT) == 0)
		{
			
			if(received_event == BUTTON_PRESS)
			{
				gpio_pin_set_dt(&red_led_spec, false);			
				speed_index = (speed_index + 1) % 6;
				speed = speed_arr[speed_index];
			}
		}
		
		
		wait_time = speed;

		k_sleep(K_MSEC(wait_time));
		//		gpio_pin_set(buz, BUZZER_PIN, status);
		gpio_pin_set(dev, PIN, status);
		gpio_pin_set(dev2, BLUE_PIN, status);
		status = !status;
	}
}

void ISR_button_handler(const struct device *port, struct gpio_callback *cb, gpio_port_pins_t pins)
{
	event_t data;
	data = BUTTON_PRESS;
	k_msgq_put(&event_queue, &data, K_NO_WAIT );
	gpio_pin_set_dt(&red_led_spec, true);			

}

void polling_thread(void *__button, void *__pin, void *dummy)
{
	uint32_t i = 0;
	struct device *button;
	gpio_pin_t pin = *(gpio_pin_t *)__pin;
	button = (struct device *)__button;

	ARG_UNUSED(dummy);

	while (true)
	{

		if (gpio_pin_get(button, pin) == true)
		{
			k_sleep(K_MSEC(50));
			if (gpio_pin_get(button, pin) == true)
			{
				i = (i + 1) % 5;
				k_mutex_lock(&mutex, K_FOREVER);
				speed = speed_arr[i];
				k_mutex_unlock(&mutex);
			}
		}
	}
}
