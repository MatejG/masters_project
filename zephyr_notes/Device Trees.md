# Device trees

## What are device trees?

Device trees are in essence, a handy way of describing peripherals and devices on a given system. By using a special device tree syntax, a device tree compiler turns that device tree source file into a header file with device data used in C. 

The main file type used in device trees is a .dts file or a "device tree source file". Dts files can be concatenated onto one another by including dtsi file ("Device tree source include)". Additionaly, a dts file can be concatenated with an overlay file, as can be seen on the following image:

![[Pasted image 20220110123436.png]]

Overlay files are typically used for describing peripherals on an external shield.

After the dts files are all collected, they are validated with device tree *bindings*. These are files that check the fields of each dts node that has the same "compatible" field as the binding file. For example in examples/gpio/buzzer we have an overlay file with buzzer node with "buzzer,generic" compatible field and coresponding buzzer binding file.

Before compiling in cmake we need to set DTC_OVERLAY_FILE variable with this line:
`set(DTC_OVERLAY_FILE dts/overlays/fer_shield.overlay)`

Here I am using FER shield overlay as an example.




	
