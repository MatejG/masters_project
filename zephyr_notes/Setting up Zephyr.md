
# Basic info about setting up Zephyr
The process of setting up Zephyr can be a daunting task when following the official documentation. 

When setting up Zephyr the first new concept is the [[West]] versioning and build tool. It is a rather complex tool but for the purpouse of this note it isnt too important.

The official zephyr getting started guide [Getting started guide](https://docs.zephyrproject.org/latest/getting_started/index.html)  is excellent to follow for downloading the necessary software and files. 

## Step by step guide
Step by step of getting a new project started:
1. Follow the [Getting started guide](https://docs.zephyrproject.org/latest/getting_started/index.html)
2. Create project structure according to [[West#Building apps and typical project structure]]
3. Setup CMakeLists.txt according to [[West#West and CMake]]
4. Setup `ZEPHYR_BASE` env variable according to [[How to define ZEPHYR_BASE env variable]]
5. Build project [[West#Building apps and typical project structure]]
6. Flash project [[West#Flashing our program to target MCU]]