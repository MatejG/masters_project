# Defining ZEPHYR_BASE env variable

When we talk about ZEPHYR_BASE we are refering to the path to the git repo we cloned when completing the [Getting started guide](https://docs.zephyrproject.org/latest/getting_started/index.html) from the Zephyr official web site. 

Zephyr repo has a file structure like this: 

zephyr_repo
├── bootloader
├── build
├── modules
├── tools
├── .vscode
├── .west
└── zephyr

We need to set the path to our Zephyr repo in an enviroment variable so that West can build our Zephyr apps anywhere in our system.  

To do that we must open our bash startup script `~/.bashrcc` that runs when the PC is turned on. In that script, we add the following line at the bottom of the source code :

* `export ZEPHYR_BASE='<path to zephyr_repo>/zephyr_repo/zephyr'`

For example on my PC the path is :

* `export ZEPHYR_BASE='/home/matejg/Documents/faks/zephyr_repo/zephyr'`