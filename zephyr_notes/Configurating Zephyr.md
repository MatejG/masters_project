# Prj.conf file 

As mentioned in [[West#Building apps and typical project structure]] the typical Zephyr project structure has a **prj.conf** file. This file is used for tweaking Zephyr kernel settings.
For example we can configure thread priorities and scheduling options. We can also enable or disable communication peripherals. 