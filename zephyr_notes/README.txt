This directory is best viewed by installing Obsidian, a markdown formatting tool
that supports linking multiple md files in the same directory. Those links can later be graphically represented
in the program.

Obsidian can be installed from their official web site:

https://obsidian.md/

Install the program for your OS, and open this directory as an "Obsidian vault". These Obsidian vault directories can be 
recognized by having a ".obsidian" hidden directory in them.