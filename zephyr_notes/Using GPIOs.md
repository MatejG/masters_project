# Summary of using GPIOS in Zephyr


## Necessary prerequisites 
To use GPIOs in Zephyr, we first have to describe the peripheral we are going to use by defining a dts node in ,for example, an overlay file.  After that, we create a binding YAML file that validates the dts node we defined.


## Getting data from dts nodes

In our device node, we defined the gpio controller we will be using in one of the fields. The dts compiler has turned our dts file into a C header file called "devicetree_unfixed.h" located at "build/zephyr/include/generated" in our zephyr project. Since the generated macros are generally quite long, Zephyr provides us with helper macros for easier access to these long macros. Here is an example of accesing a buzzer node:

`
#define BUZZER_NODE DT_NODELABEL(buzzer)
#define BUZZER_PORT DT_GPIO_LABEL(BUZZER_NODE, gpios)
#define BUZZER_PIN DT_GPIO_PIN (BUZZER_NODE, gpios)
`

The first macro will fetch us a macro that identifies our node based on the label we provide. The second one will get the first member of the "gpios" field array of the node we provided. That member will actually be used to get our gpio device structure so we can finally start toggling pins. The last macro gets us the second member of the "gpio" field which is actually the pin on the gpio port we will use.

## Using the data in our program

After we defined the macros and got our data we call the following function in our main function:

`static const struct device *buz;
buz = device_get_binding(BUZZER_PORT);
`

The returned structure has the following definition:

`
struct device {const char *name;
const void *config;
const void *api;
struct device_state * const state;
void * const data;
...
}`

The name variable is used to find the reference to the structure via macros. Data variable holds an adress to some data regarding a particular device. For example in our gpio data variable holds the adress of a gpio data struct that holds a local mask copy for the current state of the gpio controller.
The api variable holds the adress of a struct containing multiple functions implementing typical gpio functionalities (pin configuration, toggling pins...). Zephyr is designed so that on every MCU, the structures associated with different devices are updated with correct driver functions. For example configuring a pin on STM32 will eventually through the api layers call `static int gpio_stm32_config(const struct device *dev, gpio_pin_t pin, gpio_flags_t flags)`. That function will then access STM32s HAL macros and access individual registers specific to the STM32 MCU family.

After getting the device structure, we can finally start using the GPIO with coresponding API functions. First we configure the gpio device with the following function:

`ret = gpio_pin_configure(buz, BUZZER_PIN,GPIO_OUTPUT_INACTIVE);`

With GPIO_OUTPUT_INACTIVE flag we configure that the default state of the pin is low. 

To toggle the gpio pin we use the following function:

`gpio_pin_set(buz, BUZZER_PIN, true);
k_msleep(500);
gpio_pin_set(buz, BUZZER_PIN, false);
`











