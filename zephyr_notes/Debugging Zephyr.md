# Debugging Zephyr


## Common problems encountered

Although my debugging software was installed correctly, for some reason if the cable connected is disconnected and then reconnected, gdb will not open when using **Cortex Debug** vscode extension. **The solution is to delete the build project, rebuild it from scratch, disconnect and reconnect the cable, run `west flash` and only then hit F5 for debugging**.