
# Table of contents

Root node for researching Zephyr OS. The notes are organized into thematic sections that explain and test the inner workings of the OS.

1. [[Setting up Zephyr]]
2. [[Configurating Zephyr]]
3. [[Debugging Zephyr]]
4. [[Threads in Zephyr]]
5. [[Device Trees]]
6. [[Using GPIOs]]

