# Brief overview of West 
	
## What is West?
West is an advanced versioning and build tool that the creators of Zephyr made to ease the use of their product OS. 


## Building apps and typical project structure

When building an embedded app, the command we use is :
* `west build -b <Board_Name>` 
The command is invoked in the project root folder, for example a typical project structure looks like this:

Proj_root
├── CMakeLists.txt
├── prj.conf
└── src
    └── main.c
	
We would invoke the command in ../Proj_root/.

### Common build errors

If for some reason we get a build error, a valid idea is to try a pristine build with the `-p` option, that rebuilds the whole OS from scratch.

When we move out project directory to another location, a common error is a CMake error that our path to project directory has changed. To fix this we only have to remove the build directory with:

* `rm -rf build`

And then rebuild with :

* `west build`

## West and CMake

West is a tool that is dependant on the [[CMake]] build system. Typically for building a zephyr application we use the following template:

`
	cmake_minimum_required(VERSION 3.13.1)
	set(BOARD nucleo_f401re)
	find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
	project(app)
	target_sources(app PRIVATE src/main.c)

`

The two important points about this template are the `BOARD` cmake variable and the `ZEPHYR_BASE` enviroment variable:

* `BOARD` is used for defining the board on which our software will be flashed. It replaces the -b definition in `west build -b <BOARD_NAME>` command so we can just type `west build`
*  `ZEPHYR_BASE` is an enviroment variable used by west to find zephyr source code to be built with our user application.[[How to define ZEPHYR_BASE env variable]]

## Flashing our program to target MCU

Flashing the program to our MCU is easy. All we have to do is to invoke:
* `west flash` in our project root




