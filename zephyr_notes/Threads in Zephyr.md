# Basic info about threads

This chapter contains a brief overview of threads in Zephyr OS.


## Thread states
In Zephyr there are several states that a thread can find itself . The following diagram illustrates these states:

![[zephyr_states.png]]


## Thread creation

Threads can be created in two ways :

1. In main function by using `k_thread_create()`
2. Outside main funtion by using `K_THREAD_STACK_DEFINE();` macro

### Creating threads in main

If we use the first method , we have to define the stack area for the thread we are creating. We do that outside of main with :

* `K_THREAD_STACK_DEFINE(stack_desc, MY_STACK_SIZE);`

Important thing to notice is that the first argument is not a declared variable, the macro will define a structure named `stack_desc` that will be aligned in memory according to our needs. 

Next we need to declare a thread descriptor structure like this:

*  `static struct k_thread thread_desc`

After that in main function we call `k_thread_create` for example like this:

* `threadA_id = k_thread_create(&thread_desc, stack_desc, K_THREAD_STACK_SIZEOF(stack_desc), threadA, NULL,NULL,NULL, MY_PRIO, MY_FLAGS, delay);`

The function returns a thread id that can be used in some other kernel functions. 
We pass in the following arguments :

- `new_thread` – Pointer to uninitialized struct k_thread
-   `stack_desc` – Pointer to the stack space.
-   `K_THREAD_STACK_SIZEOF(stack_desc)` – Stack size in bytes.
-   `threadA` – Thread entry function.
-   `NULL` – 1st entry point parameter.
-   `NULL` – 2nd entry point parameter.
-   `NULL` – 3rd entry point parameter.
-   `MY_PRIO` – Thread priority.
-   `MY_FLAGS` – Thread options.
-   `delay` – Scheduling delay, or K_NO_WAIT (for no delay), K_FOREVER for infinity.

After calling this function, the created thread is placed in the ** new threads queue** as shown in [[Threads in Zephyr#Thread states]]. The thread will be placed in the **ready threads queue** after the delay set in `delay` argument has passed.

Thread funtions in Zephyr have to have the following prototype:

* `void thread_name(void *arg1, void *arg2, void *arg3)`

A common practice is to use `ARG_UNUSED()` macros to supress compiler warnings about unused arguments. For example : 

* `ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);`


	
### Creating threads outside of main 

Outside of main threads are created with the following macro:
* `K_THREAD_DEFINE(my_tid, MY_STACK_SIZE,
                threadA, NULL, NULL, NULL,
                MY_PRIORITY, 0, 0);`

Where we pass the following arguments:

- `my_tid` Thread id that is same as the one returned by `k_thread_create()`.
- `MY_STACK_SIZE` Stack size in bytes.

The rest of the arguments are analogous to `k_thread_create()` funtion described in [[Threads in Zephyr#Creating threads in main]].

## Starting threads 

If a thread is created with the delay argument set to `K_FOREVER`, the thread will stay in the **new threads queue** until it is moved to the **ready threads queue** by the following function :

* `k_thread_start(&thread_desc);`

Where the argument is the thread descriptor initialized with `k_thread_create()` function. 

## Thread priorities

A thread’s priority is an **integer value**, and can be either **negative or non-negative**. **Numerically lower priorities takes precedence over numerically higher values**. For example, the scheduler gives thread A of priority 4 _higher_ priority over thread B of priority 7; likewise thread C of priority -2 has higher priority than both thread A and thread B.

The scheduler distinguishes between two classes of threads, based on each thread’s priority.

-   A **cooperative** thread has a negative priority value. Once it becomes the current thread, a cooperative thread remains the current thread until it performs an action that makes it unready.
    
-   A **preemptible** thread has a non-negative priority value. Once it becomes the current thread, a preemptible thread may be supplanted at any time if a cooperative thread, or a preemptible thread of higher or equal priority, becomes ready.