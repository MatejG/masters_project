# Notes about this project

This is a masters project whose main purpose is to give a closer look into the functioning of Zephyr OS on a student level.
The repository is for now composed of the following folders:

* **examples** - Contains project files for examples used to test Zephyr OS
* **zephyr_notes** - Contains an Obsidian vault with markdown documents explaining the setup and basic concepts od Zephyr

Still a WIP so there are bound to be some changes...
